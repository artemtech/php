# PHP-FPM image with composer + nginx included

best suitable with laravel projects

## Contents
- [7.3-fpm-composer1](./Dockerfile)
- [7.3-fpm-composer2](./7.3-composer2/)
- [7.3-fpm-nginx-composer1](./7.3-nginx-composer1/)
- [7.3-fpm-nginx-composer2](./7.3-nginx-composer2/)


## How to Build Manually
For example, you might want to build 7.3 composer2:
```bash
cd 7.3-composer2/
docker build -t your_desired_image_name:version $(pwd)
```

## Using this image from gitlab
`docker pull registry.gitlab.com/artemtech/php:<variant>`
example:  
`docker pull registry.gitlab.com/artemtech/php:7.3-fpm-nginx-composer1`
