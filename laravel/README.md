# Usage
- Copy Dockerfile, .dockerignore and docker folder into your laravel project
- set php and nginx.conf as you want (in the docker folder)
- Build your app using: 
```
docker build -t yourappname:version .
```

# using docker-compose
- env file as bind mount
```yaml
version: "3.5"
services:
    app:
        image: yourapp:version
        environment:
            SECRET_NAME: myapp_env                          # must be same as your secrets file name
            CONTAINER_ROLE: webserver
        volumes:
            - ./myapp_env:/run/secrets/myapp_env
            - /var/log/myapp:/var/www/html/storage/logs     # change /var/log/myapp to path to store error logs from laravel
        ports:
            - 8000:80
```
- using env as a docker secret
```yaml
version: "3.5"
services:
    app:
        image: yourapp:version
        environment:
            SECRET_NAME: myappsecret1                     # must be same as your secrets name
            CONTAINER_ROLE: webserver
        secrets:
            - myappsecret
        ports:
            - 8000:80
        volumes:
            - /var/log/myapp:/var/www/html/storage/logs   # change /var/log/myapp to path to store error logs from laravel
secrets:
    myappsecret:
        name: myappsecret1
        file: ./mysecret
```
